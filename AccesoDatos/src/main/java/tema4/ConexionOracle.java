package main.java.tema4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConexionOracle {
	public static void main(String[] args) {

		Connection conn1 = null;
		Connection conn2 = null;
		Connection conn3 = null;

		try {
			// Se encarga de registrar el driver de oracle, es opcional
			Class.forName("oracle.jdbc.OracleDriver");

			// Metodo 1
			String url = "jdbc:oracle:thin:ilerna/contrasena@localhost:1521:tema4";
			conn1 = DriverManager.getConnection(url);
			if (conn1 != null) {
				System.out.println("Conectado usando el metodo 1");
			}

			// Metodo 2
			String url2 = "jdbc:oracle:thin:@tema4";
			String usuario = "ilerna";
			String pass = "contrasena";

			conn2 = DriverManager.getConnection(url2, usuario, pass);
			if (conn2 != null) {
				System.out.println("Conectado usando el metodo 1");
			}

			// Metodo 3
			String url3 = "jdbc:oracle:thin:@tema4";
			Properties properties = new Properties();
			properties.put("user", "ilerna");
			properties.put("password", "contrasena");
			properties.put("defaultRowPrefetch", "20");

			conn3 = DriverManager.getConnection(url3, properties);

			if (conn3 != null) {
				System.out.println("Conectado usando el metodo 1");
			}
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn1 != null && !conn1.isClosed()) {
					conn1.close();
				}
				if (conn2 != null && !conn2.isClosed()) {
					conn2.close();
				}
				if (conn3 != null && !conn3.isClosed()) {
					conn3.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public Connection realizarConexion() {

		Connection conn1 = null;

		try {
			// Se encarga de registrar el driver de oracle, es opcional
			Class.forName("oracle.jdbc.OracleDriver");

			// Metodo 1
			String url = "jdbc:oracle:thin:ilerna/contrasena@localhost:1521:tema4";
			conn1 = DriverManager.getConnection(url);
			if (conn1 != null) {
				System.out.println("Conectado usando el metodo 1");
			}
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn1 != null && !conn1.isClosed()) {
					conn1.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}

		}
		return conn1;
	}
}

package main.java.tema4;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Struct;

/**
 * Clase que se encargara de realizar todas las operaciones con la base de datos.
 * @author Laura Gatius
 *
 */
public class EjercicicosDaoImpl {

	ConexionOracle aux = null;
	Connection conn = null;

	/**
	 * Método que realiza la creación de un objeto y de una tabla en la base de datos Oracle. Retorna true si ha ido bien.
	 * @return result.
	 */
	public boolean creacionTablasObjetos() {
		Statement stmt = null;
		aux = new ConexionOracle();
		conn = aux.realizarConexion();
		boolean result = true;
		try {
			stmt = conn.createStatement();

			String sql;
			sql = "CREATE Or Replace TYPE ProductoType AS OBJECT (id NUMBER," + " nombre        VARCHAR2(15),"
					+ "  descripcion VARCHAR2(22)," + " precio       NUMBER(5, 2)," + " dias_validez  NUMBER" + ");";

			stmt.execute(sql);

			sql = "CREATE TABLE productos (" + "    producto           ProductoType,"
					+ "    contador             NUMBER);";
			stmt.execute(sql);

		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		}
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean seleccionObjetos() {
		Statement stmt = null;
		aux = new ConexionOracle();
		conn = aux.realizarConexion();
		boolean result = true;
		try {
			stmt = conn.createStatement();

			String sql = "select product FROM productos; ";

			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Struct productos = (Struct) rs.getObject(1);
				Object[] elements = productos.getAttributes();
				int id = (int) elements[0];
				String nombre = (String) elements[1];
				String descripcion = (String) elements[2];
				int precio = (int) elements[3];
				int validez = (int) elements[4];
				
				System.out.println("Id Producto: " + id);
				System.out.println("Nombre: " + nombre);
				System.out.println("Descripcion: " + descripcion);
				System.out.println("Precio: " + precio);
				System.out.println("Validez: " + validez);
			}

		
			rs.close();
		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		}
		try {
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

}

package main.java.tema2.consultas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.objetos.Estudiante;
import main.java.tema2.utils.ConnectorBBDD;

public class Select {

	static ConnectorBBDD conector = null;

	public void consultaSencilla() {

		Connection conn = null;
		Statement stmt = null;
		try {
			// Paso 1: Realizamos la conexi�n
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");

			// Paso 2: Ejecutamos la query
			String sql = "SELECT * FROM Estudiante";
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// Paso 3: Imprimimos el resultado por pantalla
			while (rs.next()) {
				
				// Obtenemos los datos de cada columna
				int id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String apellido = rs.getString("apellido");
				String dni = rs.getString("dni");

				//Mostramos los valores
				System.out.print("ID: " + id);
				System.out.print(", Nombre: " + nombre);
				System.out.print(", Apellido: " + apellido);
				System.out.println(", DNI: " + dni);
			}
			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Bloque para cerrar las conexiones
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se) {
				System.out.println("No se ha podido cerrar la conexion");
			}
		}
	}

	public void consultaDinamica() {
		Connection conexion = null;
		PreparedStatement sentencia = null;
		Estudiante estudiante = new Estudiante();
		try {
			// Paso 1: Realizamos la conexi�n
			conexion = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");

			// Paso 2: Ejecutamos la query
			String consulta = "select id, nombre, dni from estudiante where nombre = ? ";
			sentencia = conexion.prepareStatement(consulta);
			sentencia.setString(1, estudiante.getNombre());

			// Paso 3. Ejecución
			ResultSet rs = sentencia.executeQuery();
			while (rs.next()) {
				// Paso 4. obtenemos los parámetros
				int id = rs.getInt("id");
				String dni = rs.getString("dni");
				String nombre = rs.getString("nombre");

				// Paso 5. Mostrar resultados
				System.out.print("ID: " + id);
				System.out.print(", DNI: " + dni);
				System.out.print(", NOMBRE: " + nombre);
			}

			rs.close();
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Bloque para cerrar las conexiones
			try {
				if (sentencia != null)
					sentencia.close();
			} catch (SQLException se) {
				System.out.println("No se ha podido cerrar la conexion");
			}
		}

	}
}

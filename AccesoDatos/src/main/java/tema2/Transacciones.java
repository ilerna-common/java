package main.java.tema2;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

import main.java.tema2.utils.ConnectorBBDD;



public class Transacciones {

	static ConnectorBBDD conector = null;
	static Connection conn = null;

	public void ejemploSavePoint() throws SQLException {
		Savepoint savepoint1 = null;
		
		try{
			// Paso 1. Previamente habremos realizado la conexion
			//Paso 2. Creamos un nuevo objeto Statement y estableemos la transaccion manual
		conn.setAutoCommit(false);
		Statement stmt = conn.createStatement();
		//Paso 3. Creamos un nuevo Savepoint
		savepoint1 = conn.setSavepoint("punto de backup");

		String consulta = "INSERT INTO Estudiante (id, dni, nombre, apellido, edad) VALUES (22, '11111111H', 'Zara', 'Ali', 18)";
		// Paso 4. Ejecuci�n
		stmt.executeUpdate(consulta);

		//Paso 5. Prueba de insert mal hecho
		consulta = "INSERTED IN Estudiante VALUES (107, 22, 'Juan', 'ejemplo')";
		stmt.executeUpdate(consulta);
		// Paso 6. Haremos commit siempre que no se produzca error.
		 conn.commit();

		}catch(SQLException se){
		   // Controlamos error y si se produce vuelve al punto de guardado
		   conn.rollback(savepoint1);
		}

	}
	

	public void ejemploTransaccion() {
		Statement stmt = null;
		try {
			//Paso 1. Establecemos conexion
			conn.setAutoCommit(false);
			stmt = conn.createStatement();

			String sql = "INSERT INTO Estudiante (id, dni, nombre, apellido, edad) VALUES (1, '20', 'Julio', 'Luz', '19')";
			stmt.executeUpdate(sql);  

			//Paso 2. Ejemplo de sentencia erronea que bloqueara la transaccion
			sql = "INSERTED IN Estudiante VALUES (107, 22, 'Juan', 'ejemplo')";
			stmt.executeUpdate(sql);

			//Paso 3. Realizamos commit si no se produce ningun error
			conn.commit();
		}catch (Exception e) {
			System.out.println("Se ha producido un error: " + e.getMessage());
		}
		finally{
			//Cerramos la connexion
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
		

	}

}

package main.java.tema2.descripcionDatos;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.tema2.utils.ConnectorBBDD;



public class Alter {

	static ConnectorBBDD conector = null;
	Connection conn = null;

	public void modificarBaseDatos() throws Exception {
		Statement stmt = null;
		try {
			conector = new ConnectorBBDD();
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			stmt = conn.createStatement();
			String sql = "ALTER DATABASE ilerna MODIFY NAME = tema2";
			stmt.executeUpdate(sql);
		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la inserci�n
			se.printStackTrace();
		}catch (Exception e) {
			System.out.println("Se ha producido un error.");
		}
		finally {
			stmt.close();
			conn.close();
		}
	}

	
	public void modificarTabla() throws Exception {
		Statement stmt = null;
		try {
			//Paso 1: Realizamos la conexion
			conector = new ConnectorBBDD();
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			//Paso 2: Preparamos el objeto Statement
			stmt = conn.createStatement();
			//Paso 3:Modificacion de la base de datos, borrar 2 columnas
			String sql = "ALTER TABLE estudiante DROP COLUMN dni, DROP COLUMN edad;";
			stmt.executeUpdate(sql);
		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		} catch (Exception e) {
			System.out.println("Se ha producido un error.");
		}
		finally {
			stmt.close();
			conn.close();
		}
	}
	
	public void anadirComentarioTabla() throws Exception {
		Statement stmt = null;
		try {
			//Paso 1: Realizamos la conexion
			conector = new ConnectorBBDD();
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			//Paso 2: Preparamos el objeto Statement
			stmt = conn.createStatement();
			//Paso 3:Modificacion de la base de datos, añadir un comentario
			String sql = "ALTER TABLE estudiante COMMENT = 'Comentario de prueba'";
			stmt.executeUpdate(sql);
		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		} catch (Exception e) {
			System.out.println("Se ha producido un error.");
		}
		finally {
			stmt.close();
			conn.close();
		}
	}
	
	public void anadirColumna() throws Exception {
		Statement stmt = null;
		try {
			//Paso 1: Realizamos la conexion
			conector = new ConnectorBBDD();
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			//Paso 2: Preparamos el objeto Statement
			stmt = conn.createStatement();
			//Paso 3:Modificacion de la base de datos, añadir una columna a una tabla
			String sql = "ALTER TABLE estudiante ADD Email varchar(255);";
			stmt.executeUpdate(sql);
		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		} catch (Exception e) {
			System.out.println("Se ha producido un error.");
		}
		finally {
			stmt.close();
			conn.close();
		}
	}
}

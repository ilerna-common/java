package main.java.tema2.descripcionDatos;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.tema2.utils.ConnectorBBDD;

public class Create {
	
	static ConnectorBBDD conector = null;
	
	public void crecionBaseDeDatos() throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try {
			//Paso 1.Previamente habremos realizado la conexión
			conn = conector.conector();
			
			//Paso 2. Creamos un nuevo objeto con la conexión
			stmt = conn.createStatement();
			
			//Paso 3. Definimos la sentencia de crear una nueva base de datos
			String sql = "CREATE DATABASE ejemplo";
			
			//Paso 4. Ejecutar la sentencia
			stmt.executeUpdate(sql);
	
		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		}catch(Exception e){
			//Gestionamos los posibles errores
			e.printStackTrace();
		}finally{
			//Paso 5. Cerrar el objeto en uso y la conexión
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
	}
	
	public void crecionTablas() throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try {
			//Paso 1.Previamente habremos realizado la conexión
			conn = conector.conector();
			
			//Paso 2. Creamos un nuevo objeto con la conexión
			stmt = conn.createStatement();
			
			//Paso 3. Definimos la sentencia de crear una nueva base de datos
			String sql = "CREATE TABLE estudiante (" + 
					"  id INT PRIMARY KEY AUTO_INCREMENT;" + 
					"  dni VARCHAR(50) NOT NULL," + 
					"  nombre VARCHAR(50) NOT NULL," + 
					"  apellido VARCHAR(100)," + 
					"  edad INT DEFAULT 10;" + 
					");";

			
			//Paso 4. Ejecutar la sentencia
			stmt.executeUpdate(sql);
	
		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		}catch(Exception e){
			//Gestionamos los posibles errores
			e.printStackTrace();
		}finally{
			//Paso 5. Cerrar el objeto en uso y la conexión
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
	}

}

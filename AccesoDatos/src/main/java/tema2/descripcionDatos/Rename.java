package main.java.tema2.descripcionDatos;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.tema2.utils.ConnectorBBDD;

public class Rename {


	static ConnectorBBDD conector = null;
	Connection conn = null;
	Statement stmt = null;

	public void renameTabla() {
		
		try {
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");

			// Paso 2. Crear objeto y llamar a la conexion
			stmt = conn.createStatement();
			
			// Paso 3. Crear estructura de la sentencia
			String sql = "RENAME TABLE estudiante to prueba;";
			
			// Paso 4. Ejecucion
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Paso 5. Cerrar objetos abiertos
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

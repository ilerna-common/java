package main.java.tema2;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import main.java.objetos.Estudiante;
import main.java.tema2.utils.ConnectorBBDD;

public class Procedimiento {

	static ConnectorBBDD conector = null;

	public void procedimientoLlamada() throws Exception {
		Connection conn = null;
		CallableStatement llamadaProcedure =null;
		Estudiante estudiante = new Estudiante();

		try {
			// Paso 1: Realizamos la conexi�n
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			String sql = "{call ObtenerDatosEstudiante (?,?)}";
			// Paso 2: Llamada al procedimiento almacenado, tiene que existir en la BBDD
			llamadaProcedure = conn.prepareCall(sql);
			
			//Paso 3: Definimos el parametro OUT
			llamadaProcedure.registerOutParameter(2, Types.VARCHAR);
			//Paso 4: definimos el parametro IN
			llamadaProcedure.setInt(1, estudiante.getId());

			//Paso 5:  Ejecuta el procedimiento almacenado
			boolean resultado = llamadaProcedure.execute();
			if(resultado) {
				ResultSet lista = llamadaProcedure.getResultSet();
				while (lista.next()) {
			      //Obtenemos el DNI 
					String dni = lista.getString("dni");
					System.out.println(dni);
			    }
			}
		} catch (SQLException ex) {
			System.out.println("Se ha producido un error en la ejecucion de la SQL: " + ex.getMessage());
		} finally {
			try {
				llamadaProcedure.close();
			} catch (SQLException ex) {
				System.out.println("Se ha producido un error al cerrar la conexion: " + ex.getMessage());
			}
		}
	}
	
	public void crearStatement() throws Exception {
		Connection conn = null;
		Statement stmt = null;

		try {
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			//Paso2: definimos el procedimiento
			String procedure = "DELIMITER $$" + 
					"DROP PROCEDURE IF EXISTS `ILERNA`.`ObtenerDatosEstudiante` $$" + 
					"CREATE PROCEDURE `ILERNA`.`ObtenerDatosEstudiante` " + 
					"   (IN ES_ID INT, OUT ES_DNI VARCHAR(255))" + 
					"BEGIN" + 
					"   SELECT DNI INTO ES_DNI" + 
					"   FROM ESTUDIANTE" + 
					"   WHERE ID = ES_ID;" + 
					"END $$" + 
					"DELIMITER ;";
			// Paso 3: Creamos el objeto Statement de la conexion
			stmt = conn.createStatement();
			//Paso 4 : ejecutamos la sql
			stmt.execute(procedure);
		} catch (SQLException ex) {
			System.out.println("Se ha producido un error en la ejecucion de la SQL: " + ex.getMessage());
		} finally {
			try {
				stmt.close();
			} catch (SQLException ex) {
				System.out.println("Se ha producido un error al cerrar la conexion: " + ex.getMessage());
			}
		}
	}
}
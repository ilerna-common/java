package main.java.tema2.modificacionDatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.objetos.Estudiante;
import main.java.tema2.utils.ConnectorBBDD;

public class Delete {

	static ConnectorBBDD conector = null;
	Connection conn = null;
	Statement stmt = null;

	public void borradoEstatico() {

		try {
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");

			// Paso 2. Crear objeto y llamar a la conexion
			stmt = conn.createStatement();

			// Paso 3. Crear estructura de la sentencia
			String sql = "DELETE FROM estudiante  WHERE dni = '00000000T'";

			// Paso 4. Ejecucion
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Paso 5. Cerrar objetos abiertos
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void borradoDinamico() {
		PreparedStatement sentencia = null;
		try {
			// Paso 1. Previamente habremos realizado la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			// Paso 2. Crear estructura de la sentencia, ponemos interrogantes a los datos
			// que seran dinamicos

			String consulta = "DELETE FROM estudiante  WHERE dni =?";

			// Paso 3. Pasamos la consulta al objeto PreparedStatement
			sentencia = conn.prepareStatement(consulta);

			// Paso 4. Asignamos los valores del objeto que queramos guardar
			// Imaginiemos que el objeto estudiante esta ya declarado y con datos.

			Estudiante estudiante = new Estudiante();
			sentencia.setString(1, estudiante.getDni());
			// Paso 5. Ejecucion
			sentencia.execute();
		} catch (SQLException e) {
			System.out.println(e.getCause());
		}catch (Exception e) {
			System.out.println(e.getCause());
		} finally {
			// Paso 6. cerramos la conexion y el objeto en uso
			try {
				sentencia.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

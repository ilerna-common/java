package main.java.tema2.modificacionDatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import main.java.objetos.Estudiante;
import main.java.tema2.utils.ConnectorBBDD;

public class Insert {

	static ConnectorBBDD conector = null;

	public void sentenciaEstatica() throws Exception {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			String sql = "INSERT INTO Estudiante (id, dni, nombre, apellido, edad) VALUES (22, '11111111H', 'Zara', 'Ali', 18)";
			stmt.executeUpdate(sql);

		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la inserci�n
			se.printStackTrace();
		}catch(Exception e){
			//Gestionamos los posibles errores
			e.printStackTrace();
		}finally{
			//Cerramos la connexion
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
	}
	
	public void sentenciaDinamica() throws Exception {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			String sql = "INSERT INTO Estudiantes (id, dni, nombre, apellido, edad) VALUES (?, ?, ?, ?, ?)";
			//Imaginemos que viene con datos
			Estudiante estudiante = new Estudiante();
			
			//Prepararemos la query para que coja los datos de manera dinamica.
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, estudiante.getId());
			stmt.setString(2, estudiante.getDni());
			stmt.setString(3, estudiante.getNombre());
			stmt.setString(4, estudiante.getApellido());
			stmt.setInt(5, estudiante.getEdad());
			stmt.executeUpdate(sql);


		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		}catch(Exception e){
			//Gestionamos los posibles errores
			e.printStackTrace();
		}finally{
			//Cerramos la connexion
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
	}

}

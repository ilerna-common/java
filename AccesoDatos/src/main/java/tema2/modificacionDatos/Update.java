package main.java.tema2.modificacionDatos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import main.java.objetos.Estudiante;
import main.java.tema2.utils.ConnectorBBDD;

public class Update {

	static ConnectorBBDD conector = null;

	public void sentenciaEstatica() throws Exception {
		Connection conn = null;
		Statement stmt = null;
		try {
			// Paso 1: Realizamos la conexion
			//Paso 2. Crear objeto y llamar a la conexión
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			//Paso 3. Crear estructura de la sentencia
			String sql = "UPDATE estudiante SET dni = '00000000T'  WHERE id = '12'";
			//Paso 4. Ejecución 
			stmt = conn.createStatement();


		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		}catch(Exception e){
			//Gestionamos los posibles errores
			e.printStackTrace();
		}finally{
			//Paso 5. Cerrar objetos abiertos
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
	}
	
	public void sentenciaDinamica() throws Exception {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Paso 1: Realizamos la conexion
			conn = conector.conector();
			System.out.println("Nos hemos conectado a la BBDD");
			
			//Paso 2. Crear estructura de la sentencia, ponemos interrogantes a los datos
			//que serán dinamicos

			String sql = "UPDATE estudiante SET dni = ? WHERE id = ?";
			
			//Estos datos en una aplicación iran variando, ahora los seteamos para que contenga valor
			Estudiante estudiante = new Estudiante();
			estudiante.setId(1);
			estudiante.setDni("00000000T");
			
			//Prepararemos la query para que coja los datos de manera dinamica.
			stmt = conn.prepareStatement(sql);
			
			// Paso 4. Asignamos los valores del objeto que queramos guardar 
			stmt.setString(1, estudiante.getDni());
			stmt.setInt(2, estudiante.getId());

			//Paso 5. Ejecución 
			stmt.execute();

		}catch(SQLException se){
			//Gestionamos los posibles errores que puedan surgir durante la ejecucion de la insercion
			se.printStackTrace();
		}catch(Exception e){
			//Gestionamos los posibles errores
			e.printStackTrace();
		}finally{
			//Cerramos la connexion
			try{
				if(stmt!=null)
					stmt.close();
					conn.close();
			}catch(SQLException se){
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}
	}

}

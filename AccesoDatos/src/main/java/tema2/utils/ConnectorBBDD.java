package main.java.tema2.utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectorBBDD {

	//Declaramos los diferentes objetos que nos permitiran realizar la conexi�n
	
	private Connection connect = null;
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/ilerna";
	static final String USUARIO = "alumno";
	static final String CONTA = "password";
	
	public Connection conector() throws Exception {
		try {
			// Esto se encarga de cargar el driver de MySQL
			Class.forName(JDBC_DRIVER);
			// Establecemos la conexion
			connect = DriverManager.getConnection(DB_URL + "user=" + USUARIO + " &password=" + CONTA);

		} catch (Exception e) {
			throw e;
		} finally {
			connect.close();
		}
		return connect;
	}

}




package main.java.tema5;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.exist.xmldb.EXistResource;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;

public class ConexionDB {

	public void connection()
			throws XMLDBException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		Collection col = null;
		XMLResource res = null;
		try {
			Class cl;

			cl = Class.forName("org.exist.xmldb.DatabaseImpl");

			Database database = (Database) cl.newInstance();
			database.setProperty("create-database", "true");
			DatabaseManager.registerDatabase(database);

		} finally {
			// dont forget to clean up!

			if (res != null) {
				try {
					((EXistResource) res).freeResources();
				} catch (XMLDBException xe) {
					xe.printStackTrace();
				}
			}

			if (col != null) {
				try {
					col.close();
				} catch (XMLDBException xe) {
					xe.printStackTrace();
				}
			}
		}

	}

	private static Collection getOrCreateCollection() throws XMLDBException {
		Collection coleccionPadre = DatabaseManager.getCollection("xmldb:exist://localhost:8080/ilerna/");
		final String nuevaColeccion = "ejemplo1";
		Collection coleccion = coleccionPadre.getChildCollection(nuevaColeccion);

		if (coleccion == null) {

			final CollectionManagementService mgmt = (CollectionManagementService) coleccionPadre
					.getService("CollectionManagementService", "1.0");

			coleccion = mgmt.createCollection(nuevaColeccion);
			
			// Cerramos la conexión
			coleccionPadre.close(); 

		}

	
		InputStream inputStream = new FileInputStream(new File("books.xqy"));
		XQDataSource ds = new SaxonXQDataSource();
		XQConnection conn = ds.getConnection();
		XQPreparedExpression exp = conn.prepareExpression(inputStream);
		XQResultSequence result = exp.executeQuery();

		while (result.next()) {
		   System.out.println(result.getItemAsString(null));
		}

		return coleccion;

	}

}







package main.java.tema1.tema1_6;


/**
 * Aqu� pondremos un resumen de la finalidad de esta clase
 * Ej: Clase para documentar el funcionamiento de JavaDoc
 * 
 * @author Ilerna
 *
 */
public class JavaDoc {

/**
 * Atributo nombre de la clase javaDoc
 */
private String nombre;

/**
 * <p>Ejemplo de Javadoc con tags html .
 * <a href="http://www.ilerna.es">Web ilerna</a>
 * </p>
 * @param ejemplo String que pasamos por parametro
 * @return String con el resultado del metodo
 * @see <a href="http://www.ilerna.es">Ejemplos</a>
 * @since 1.0
 */
public String ejemploMetodoJavadoc(String ejemplo) {
    // Comentario con explicaci�n de lo que se realiza en el m�todo
    return "OK";
}
	
}

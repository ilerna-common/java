package main.java.tema1.tema1_1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class CrearFicherosDirectorio {

	public static void main (String args[]) throws IOException {
		
//		//Paquete file de java.io
//		//Al crear el fichero file se le passa por parametro un String donde s ele indica el nombre
//		//y extension del fichero. Tambien se puede indicar ruta y nombre
//		
//		//Fichero con nombre y extension
//		File fichero = new File("ejemplo1.txt");
//		fichero.createNewFile();
//		
//		//Fichero con ruta, nombre y extensi�n
		File archivo = new File("src/main/resources/crearFicheros/fichero.txt");
		if(archivo.createNewFile()) {
			System.out.println("Creado el fichero "+ archivo);
		}else {
			System.out.println("No se ha creado el fichero");
		}
		
//		
//		//Cuando queramos crear un directorio solo necesitaremos llamar a este metodo
		archivo = new File("src/main/resources/crearFicheros/directorio");
		if(archivo.mkdir()) {
			System.out.println("Creado el directorio "+ archivo);
		}
		
	

		
		
//		//Como vemos el metodo es distinto y el nombre del fichero no tiene extension
//		
//		
//		//Crear un fichero con paquete java.nio.file.Files
//		
		Path ruta = Paths.get("src/main/resources/crearFicheros/file.txt");
		Path ejemploArchivo = Files.createFile(ruta);
		System.out.println("Hemos creado un fichero "+ ejemploArchivo);
//		//Crear un directorio con paquete java.nio.file.Files
//		
		String nombre = "src/main/resources/crearFicheros/carpetaEjemplo";
		Path nombreRuta = Paths.get(nombre); 
		Files.createDirectories(nombreRuta);
		
		System.out.println("Hemos creado una carpeta "+ nombre);

	}
}

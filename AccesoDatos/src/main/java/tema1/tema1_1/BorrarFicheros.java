package main.java.tema1.tema1_1;



import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class BorrarFicheros {

	public static void main (String args[]) throws IOException {
		
		//Paquete file de java.io
		//Al crear el fichero file se le passa por parametro un String donde s ele indica el nombre
		//y extension del fichero. Tambien se puede indicar ruta y nombre
		
		//Fichero con nombre y extension
		File archivo = new File("src/main/resources/crearFicheros/fichero.txt");

		if(archivo.delete()) {
			System.out.println("Hemos borrado un fichero");
		}
		
		//Fichero con nombre y extension
		File ejercicio2 = new File("src/main/resources/crearFicheros/fichero.txt");
		archivo.deleteOnExit();
			
				
		//Fichero con ruta, nombre y extension
		//Primero crearemos una carpeta, en este caso se crea porque no existe y sino nos dara error.
		String nombrePath = "src/main/resources/crearFicheros/directorio";
		File ficheroCarpeta = new File(nombrePath);
		ficheroCarpeta.mkdir();
		//Indicamos la ruta con el directorio y el fichero que queremos crear
		String nombrefichero = "src/main/resources/crearFicheros/ejercicio/fichero.txt";
		File ejemplo = new File(nombrefichero);
		ejemplo.createNewFile();
		
		//Cogemos la ruta y lo borramos. Si no existe dara error
		Path path = Paths.get(nombrefichero); 
		try {
			Files.delete(path);
		}catch (IOException e) {
			System.out.println("No existe la carpeta" + ficheroCarpeta);
		}
		
		
		
		
		
		
		System.out.println("Hemos borrado un fichero con ruta" + ficheroCarpeta);
	}
	
}

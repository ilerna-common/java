package main.java.tema1.tema1_1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;



public class MoverFicheros {

	
	public static void main (String args[]) throws IOException {
		
		//Paso 1: primero creamos un directorio para trabajar con los ficheros, no es obligatorio pero sera practico para este ejercicio
		File carpeta = new File("src/main/resources/crearFicheros/ejercicio");
		carpeta.mkdir();
//		//Paso2: creamos un fichero que utilizaremos para mover
		File archivo = new File("src/main/resources/crearFicheros/ejercicio/ejemplo.txt");
		archivo.createNewFile();
//		
//		//Paso 3: Ejemplo practico para mover archivos con la api java.nio
		Path destino = Files.move(Paths.get("src/main/resources/crearFicheros/ejercicio/ejemplo.txt"),  
				Paths.get("src/main/resources/crearFicheros/destinoEjemplo.txt"), StandardCopyOption.REPLACE_EXISTING); 
		System.out.println("Hemos movido el fichero. Ahora se encuentra en: " + destino.toString());
//		
//		
//		//Ejemplo practico para mover archivos con la api java.io
		 File origenArchivo = new File("src/main/resources/crearFicheros/ejercicio/ficheroOrigen.txt");
		 origenArchivo.createNewFile();
//		 
//		 //Lo que hace basicamente es renombrar el archivo en un nuevo File
		 if(origenArchivo.renameTo(new File("src/main/resources/crearFicheros/destino.txt"))){ 
			 //y luego borrar el archivo de origen.
			 origenArchivo.delete();
			 System.out.println("Se ha movido el fichero."); 
		 } else{ 
		    System.out.println("Se ha producido un error."); 
		}
	}
}

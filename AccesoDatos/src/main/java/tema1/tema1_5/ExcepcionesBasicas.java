package main.java.tema1.tema1_5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ExcepcionesBasicas {
	
	//se declara la excepcion al declarar el metodo
	private static void excepcionControladaConThrows() throws FileNotFoundException {
	    File ej = new File("not_existing_file.txt");
	    FileInputStream stream = new FileInputStream(ej);
	}

	private static void metodoConTryCatch() {
	    File ej = new File("not_existing_file.txt");
	    try {
	        FileInputStream stream = new FileInputStream(ej);
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }
	}


}

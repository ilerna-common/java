package main.java.tema1.tema1_2;

import java.io.IOException;
import java.io.RandomAccessFile;


public class AccesoDatosAleatorio {
	public static void main(String args[]) {

		//Aqui escribiremos datos en un fichero que creamos de 0
		String texto = "Prueba de informacion acceso a datos";
		writeToRandomAccessFile("prueba.txt", 10, texto);
		System.out.println("Programa de prueba de acceso de datos aleatorio : " + texto);

		//En este string guardaremos los datos leidos del fichero ejercicio.txt
		String datosLeidos = readFromRandomAccessFile("prueba.txt", 10);
		System.out.println("Acceso aleatorio programa Java: " + datosLeidos);

	}

	/*
	 * Metodo para leer des del objeto RandomAccesFile en Java
	 */
	public static String readFromRandomAccessFile(String fichero, int posicion) {
		String datos = null;
		try {
			RandomAccessFile lectura = new RandomAccessFile(fichero, "rw");

			// Se mueve a la posicion del fichero que le indiquemos
			lectura.seek(posicion);

			//Aqui leemos el string de datos desde el objeto RandomAccesFile
			datos = lectura.readUTF();

			lectura.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return datos;
	}

	/*
	 * Metodo que usaremos para escribir en el fichero
	 */
	public static void writeToRandomAccessFile(String fichero, int posicion, String datos) {
		try {
			//Creamos un nuevo fichero apto para lectura y escriptura
			RandomAccessFile archivo = new RandomAccessFile(fichero, "rw");

			// Se mueve a la posicion del fichero que le indiquemos
			archivo.seek(posicion);

			//Escribe los datos en un string
			archivo.writeUTF(datos);

			//Cerramos el proceso 
			archivo.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

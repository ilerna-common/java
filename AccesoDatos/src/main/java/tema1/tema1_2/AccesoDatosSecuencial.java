package main.java.tema1.tema1_2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;


public class AccesoDatosSecuencial {
	
	public static void main(String [] args) {
		//Primero crearemos un fichero nuevo
		
		File fichero  = new File("fichero.txt");
		//Creamos el stream que leera los datos del fichero
		PrintWriter pw;
		try {
			pw = new PrintWriter(fichero);
			pw.println("Creamos contenido para el fichero.");
			for(int i =1; i<=10; i++) {
				//Escribimos los datos al fichero
				pw.println(i + " ");
				System.out.println("El fichero se ha creado.");
			}
		}catch (FileNotFoundException e) {
			System.out.println("Se ha producido un error" + e.getMessage());
		}
	}

}

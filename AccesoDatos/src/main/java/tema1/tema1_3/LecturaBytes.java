package main.java.tema1.tema1_3;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class LecturaBytes {

	public static void main(String args[]) throws IOException {

		lecturaFileInputStream();
		lecturaInputStream();
		lecturaBufferedInputStream();
	}

	public static void lecturaFileInputStream() throws IOException {
		System.out.println("Inicio ejemplo lecturaFileInputStream");

		byte[] array = new byte[100];
		try {
			InputStream archivo = new FileInputStream("src/main/resources/ejemplo.txt");

			// Lectura de bytes des del Input Stream
			archivo.read(array);

			// Convierte los bytes en un string
			String datos = new String(array);
			System.out.println(datos);

			// Cerramos el inputStream
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			e.printStackTrace();
		}

		System.out.println("Fin ejemplo lecturaFileInputStream");
	}

	public static void lecturaInputStream() throws IOException {
		System.out.println("Inicio ejemplo lecturaInputStream");

		InputStream inputstream = null;
		try {
			inputstream = new FileInputStream("src/main/resources/ejemplo.txt");
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			e.printStackTrace();
		}

		int data = 0;
		try {
			data = inputstream.read();
		} catch (IOException e) {
			System.out.println("Se ha producido un error");
			e.printStackTrace();
		}
		while (data != -1) {
			data = inputstream.read();
			System.out.println(data);
		}
		inputstream.close();

		System.out.println("Fin ejemplo lecturaInputStream");
	}

	public static void lecturaBufferedInputStream() {
		try {

			// creamos un InputStream para coger el fichero de la ruta de nuestro proyecto
			FileInputStream fichero = new FileInputStream("src/main/resources/ejemplo.txt");

			// Creamos un BufferedInputStream y le pasamos el archivo al constructor
			BufferedInputStream bufer = new BufferedInputStream(fichero);

			// Se encarga de leer el primer byte del fichero
			int i = bufer.read();
			while (i != -1) {
				System.out.print((char) i);
				// Va leyendo cada byte del buffer
				i = bufer.read();
			}
			bufer.close();
		} catch (Exception e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
	}
}
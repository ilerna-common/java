package main.java.tema1.tema1_3;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class LecturaDatos {

	public static void main(String args[]) throws IOException {

		lecturaDatosBufferedReader();
		lecturaFileReader();
		lecturaConScanner();
		lecturaFileInputStream();
		lecturaInputStream();
		lecturaBufferedInputStream();
	}

	public static void lecturaDatosBufferedReader() throws IOException {
		System.out.println("Inicio ejemplo lecturaDatosBufferedReader");
		// Utilizamos para leer el fichero un archivo en la carpeta resources
		File file = new File("src/main/resources/ejemplo.txt");
		// Creamos el buffer
		BufferedReader reader;
		try {
			// Envolvemos el archivo dentro de un file reader
			reader = new BufferedReader(new FileReader(file));
			String datos;
			// Imprimimos los datos por consola
			while ((datos = reader.readLine()) != null)
				System.out.println(datos);
		} catch (FileNotFoundException e) {
			System.out.println("Se ha producido un error");
			e.printStackTrace();
		}
		System.out.println("Fin ejemplo lecturaDatosBufferedReader");
	}

	public static void lecturaFileReader() {
		System.out.println("Inicio ejemplo lecturaFileReader");

		// Utilizamos para leer el fichero un archivo en la carpeta resources
		FileReader fichero = null;
		try {
			fichero = new FileReader("src/main/resources/ejemplo.txt");
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			e.printStackTrace();
		}

		int i;
		try {
			while ((i = fichero.read()) != -1)
				System.out.print((char) i);
		} catch (IOException e) {
			System.out.println("Se ha producido un error");
			e.printStackTrace();
		}

		System.out.println("Fin ejemplo lecturaFileReader");
	}

	public static void lecturaConScanner() {
		System.out.println("Inicio ejemplo lecturaConScanner");

		File archivo = new File("src/main/resources/ejemplo.txt");
		Scanner lector = null;
		try {
			lector = new Scanner(archivo);
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			e.printStackTrace();
		}

		// Usamos \\Z como un delimitador
		lector.useDelimiter("\\Z");

		System.out.println(lector.next());

		System.out.println("Fin ejemplo lecturaConScanner");
	}

	public static void lecturaFileInputStream() throws IOException {
		System.out.println("Inicio ejemplo lecturaFileInputStream");

		byte[] array = new byte[100];
		try {
			InputStream archivo = new FileInputStream("src/main/resources/ejemplo.txt");

			// Lectura de bytes des del Input Stream
			archivo.read(array);

			// Convierte los bytes en un string
			String datos = new String(array);
			System.out.println(datos);

			// Cerramos el inputStream
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			e.printStackTrace();
		}

		System.out.println("Fin ejemplo lecturaFileInputStream");
	}

	public static void lecturaInputStream() throws IOException {
		System.out.println("Inicio ejemplo lecturaInputStream");

		InputStream inputstream = null;
		try {
			inputstream = new FileInputStream("src/main/resources/ejemplo.txt");
		} catch (FileNotFoundException e) {
			System.out.println("No se ha encontrado el archivo");
			e.printStackTrace();
		}

		int data = 0;
		try {
			data = inputstream.read();
		} catch (IOException e) {
			System.out.println("Se ha producido un error");
			e.printStackTrace();
		}
		while (data != -1) {
			data = inputstream.read();
			System.out.println(data);
		}
		inputstream.close();

		System.out.println("Fin ejemplo lecturaInputStream");
	}

	public static void lecturaBufferedInputStream() {
		try {

			// creamos un InputStream para coger el fichero de la ruta de nuestro proyecto
			FileInputStream fichero = new FileInputStream("src/main/resources/ejemplo.txt");

			// Creamos un BufferedInputStream y le pasamos el archivo al constructor
			BufferedInputStream bufer = new BufferedInputStream(fichero);

			// Se encarga de leer el primer byte del fichero
			int i = bufer.read();
			while (i != -1) {
				System.out.print((char) i);
				// Va leyendo cada byte del buffer
				i = bufer.read();
			}
			bufer.close();
		} catch (Exception e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
	}
}
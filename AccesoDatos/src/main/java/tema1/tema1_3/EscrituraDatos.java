package main.java.tema1.tema1_3;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;

public class EscrituraDatos {

	public static void main(String args[]) {
		escribirConFileOutputStream();
		escribirConFileWritter();
		escribirConBufferedWriter();
	}

	public static void escribirConFileOutputStream() {

		System.out.println("Inicio ejemplo escribirConFileOutputStream");

		String data = "Ejemplo de escritura de datos con FileOutputStream";

		try {
			// Creamos un FileOutputStream
			FileOutputStream archivo = new FileOutputStream("src/main/resources/escrituraDatos/outputStream.txt");

			// Creamos el stream que nos va a ayudar a escribir los datos en el fichero
			// indicado
			OutputStreamWriter escribirDatos = new OutputStreamWriter(archivo);

			// Con este método escribiremos datos en el fichero
			escribirDatos.write(data);
			// Cerramos la el writer
			escribirDatos.close();
		} catch (Exception e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
		System.out.println("Fin ejemplo escribirConFileOutputStream");
	}

	public static void escribirConFileWritter() {
		System.out.println("Inicio ejemplo escribirConFileWritter");

		String data = "Ejemplo de escritura de datos con FileWriter";

		try {
			// Creamos un FileWriter
			FileWriter output = new FileWriter("src/main/resources/escrituraDatos/fileWritter.txt");

			// Con este método escribiremos datos en el fichero
			output.write(data);

			// Cerramos la el writer
			output.close();
		} catch (Exception e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
		System.out.println("Fin ejemplo escribirConFileWritter");

	}

	public static void escribirConBufferedWriter() {
		System.out.println("Inicio ejemplo escribirConBufferedWriter");

		String data = "Ejemplo de escritura de datos con BufferedWriter";

		try {
			// Creamos un FileWriter
			FileWriter file = new FileWriter("src/main/resources/escrituraDatos/output.txt");

			// Creates a BufferedWriter
			BufferedWriter output = new BufferedWriter(file);

			// Con este método escribiremos datos en el fichero
			output.write(data);

			// Cerramos la el writer
			output.close();
		}catch (Exception e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
		System.out.println("Fin ejemplo escribirConBufferedWriter");

	}

}

package main.java.tema1.tema1_3;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class EscrituraBytes {

	public static void main(String args[]) {
		escribirConFileOutputStream();
		escribirConBufferedOutputStream();
	}

	public static void escribirConFileOutputStream() {

		System.out.println("Inicio ejemplo escribirConFileOutputStream");

		String data = "Ejemplo de escritura de datos con FileOutputStream";

		try {
			FileOutputStream output = new FileOutputStream("src/main/resources/escrituraBytes/fileOutput.txt");

			byte[] array = data.getBytes();

			//Escribimos los datos en el archivo
			output.write(array);
			
			// Cerramos el writter
			output.close();
		} catch (Exception e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
		System.out.println("Fin ejemplo escribirConFileOutputStream");
	}

	public static void escribirConBufferedOutputStream() {
		System.out.println("Inicio ejemplo escribirConBufferedOutputStream");

		String data = "Ejemplo de escritura de datos con BufferedOutputStream";

		try {
			BufferedOutputStream bufer = new BufferedOutputStream(new FileOutputStream("src/main/resources/escrituraBytes/output.txt"));
		
			bufer.write(data.getBytes());
		    bufer.close();
		} catch (IOException e) {
			System.out.println("Se ha producido un error");
			e.getStackTrace();
		}
		System.out.println("Fin ejemplo escribirConBufferedOutputStream");

	}

}

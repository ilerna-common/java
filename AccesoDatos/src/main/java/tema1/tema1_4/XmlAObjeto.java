package main.java.tema1.tema1_4;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
//from   w  w w  .j a  va 2  s  . com
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import main.java.objetos.Coche;

public class XmlAObjeto {

	public static String getXMLData() {
		return "<a><b id ='key1' value='v'></b></a>";
	}

	public static void main(String args[]) throws SAXException, IOException, ParserConfigurationException {
		// definimos un xml, funcionaria igual si crearamos un nuevo file con un xml
		// alojado en un ftp o en una maquia.
		String xml = "<?xml version= \"1.0\" encoding=\"UTF-8\"?>" + "<coches>" + "	<coche>"
				+ "		<marca>Seat</marca>" + "		<modelo>Ibiza</modelo>" + "		<color>rojo</color>"
				+ "		<matriculacion>2019</matriculacion>" + "	</coche>" + "</coches>";

		JAXBContext jaxbContext;

		try {

			jaxbContext = JAXBContext.newInstance(Coche.class);

			// Este objeto se encargara de la transformaci�n a objeto Java que le
			// indiquemos.
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Coche coche = (Coche) jaxbUnmarshaller.unmarshal(new StringReader(xml));

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Document doc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(getXMLData())));

		System.out.println(documentToString(doc));

	}

	public static String documentToString(Document document) {
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer trans = tf.newTransformer();
			StringWriter sw = new StringWriter();
			trans.transform(new DOMSource(document), new StreamResult(sw));
			return sw.toString();
		} catch (TransformerException tEx) {
			tEx.printStackTrace();
		}
		return null;
	}
}

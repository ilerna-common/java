package main.java.tema1.tema1_4;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class LeerXmlConSax {

	public static void main(String args[]) {

		try {

			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			
			SaxHelper handler = new SaxHelper();

			saxParser.parse("src/main/resources/xml/coches.xml", handler);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

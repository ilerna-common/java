package main.java.tema1.tema1_4;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHelper extends DefaultHandler {

	boolean esMarca = false;
	boolean esModelo = false;
	boolean esColor = false;
	boolean esMatriculacion = false;

	public void startElement(String uri, String localName, String elementos, Attributes atributos) throws SAXException {
		System.out.println("Inicio del elemento :" + elementos);

		switch (elementos) {
		case "marca":
			esMarca = true;
			break;
		case "modelo":
			esModelo = true;
			break;
		case "color":
			esColor = true;
			break;
		case "matriculacion":
			esMatriculacion = true;
			break;
		default:
			break;
		}
	}

	public void endElement(String uri, String localName, String elementos) throws SAXException {
		System.out.println("Fin del elemento: " + elementos);

	}

	public void characters(char ch[], int inicio, int length) throws SAXException {
		if (esMarca) {
			System.out.println("Marca: " + new String(ch, inicio, length));
			esMarca = false;
			return;
		}

		if (esModelo) {
			System.out.println("Modelo: " + new String(ch, inicio, length));
			esModelo = false;
			return;
		}

		if (esColor) {
			System.out.println("Color: " + new String(ch, inicio, length));
			esColor = false;
			return;
		}

		if (esMatriculacion) {
			System.out.println("Matriculacion: " + new String(ch, inicio, length));
			esMatriculacion = false;
			return;
		}
	}
}

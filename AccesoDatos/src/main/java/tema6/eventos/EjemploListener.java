package main.java.tema6.eventos;

import java.util.EventListener;

public interface EjemploListener extends EventListener{
	public void ejemploPracticoEvento(EjemploEvento event);
}

package main.java.tema6.introspeccion;

public class Empleado extends Persona {
	private double salario;
	public Empleado (String nombre, double salario){
		super(nombre);
		this.salario=salario;
	}
	public void setIncentivo(double incentivo){
		salario= salario+incentivo;
	}
	public String getSalario(){
		return "El salario es: " +salario;
	}
}

class Persona {
private String nombre;

	public Persona(String nombre){
		this.nombre = nombre;
	}

	public String getNombre(){
		return nombre;
	}
}

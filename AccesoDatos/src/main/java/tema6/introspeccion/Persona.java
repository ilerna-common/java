package main.java.tema6.introspeccion;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public class Persona {

	// miembro de la calse que se usa para guardar el valor de la propiedad
	private int[] objetos = { 1, 2, 3, 4 };
	private int altura = 90;
	
	
    private PropertyChangeSupport soporte = new PropertyChangeSupport(this);
    private VetoableChangeSupport pRestringida = new VetoableChangeSupport(this);

    public int getAltura() {
        return altura;
    }
    
    public void setAltura(int valor) throws PropertyVetoException {
        int oldAltura = altura;
        pRestringida.fireVetoableChange("altura",oldAltura, valor);
        altura = valor;
        
        soporte.firePropertyChange("altura", oldAltura, valor);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
    	soporte.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    	soporte.removePropertyChangeListener(listener);
    }
    
    public void addVetoableChangeListener(VetoableChangeListener listener) {
    	pRestringida.addVetoableChangeListener(listener);
    }
    
    public void removeVetoableChangeListener(VetoableChangeListener listener) {
    	pRestringida.removeVetoableChangeListener(listener);
    }
	
 // m�todos set y get de la propiedad denominada Objetos, para el array completo
 	public void setObjetos(int[] nuevoValor) {
 		objetos = nuevoValor;
 	}

 	public int[] getObjetos() {
 		return objetos;
 	}

 	// m�todos get y set para un elemento de array
 	public void setObjetos(int indice, int nuevoValor) {
 		objetos[indice] = nuevoValor;
 	}

 	public int getObjetos(int indice) {
 		return objetos[indice];
 	}
 	
 	
	
	
}

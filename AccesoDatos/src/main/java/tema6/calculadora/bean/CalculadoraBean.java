package main.java.tema6.calculadora.bean;

import javax.ejb.Stateless;

import src.java.calculadora.negocio.CalculadoraNegocio;

/**
 * Esta clase se encarga de la logica de negocio de la calculadora implementa la
 * clase CalculadoraNegocio
 * @author Laura
 *
 */
@Stateless
public class CalculadoraBean implements CalculadoraNegocio {

	/**
	 * Este método se encargará de realizar la suma entre dos valores recibidos por
	 * parametro
	 */
	@Override
	public float suma(float x, float y) {
		return x + y;
	}

	/**
	 * * Este método se encargará de realizar la resta entre dos valores que
	 * recibirá por parámetro
	 */
	@Override
	public float resta(float x, float y) {
		return x - y;
	}

	/**
	 * Este método se encargará de realizar la multiplicación entre dos valores que
	 * recibirá por parámetro
	 */
	@Override
	public float multiplicacion(float x, float y) {
		return x * y;
	}

	/**
	 * Este método se encargará de realizar la división entre dos valores que
	 * recibirá por parámetro
	 */
	@Override
	public float division(float x, float y) {
		return x / y;
	}

}

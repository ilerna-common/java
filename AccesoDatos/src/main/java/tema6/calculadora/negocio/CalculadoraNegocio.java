package main.java.tema6.calculadora.negocio;

import javax.ejb.Remote;

/**
 * Interfaz que se encarga de la logica de negocio de la calculadora
 * 
 * @author Laura
 *
 */
@Remote
public interface CalculadoraNegocio {
	/**
	 * Este método se encargará de realizar la suma entre dos valores recibidos por
	 * parametro
	 * 
	 * @param x
	 * @param y
	 * @return devuelve un float
	 */
	float suma(float x, float y);

	/**
	 * Este método se encargará de realizar la resta entre dos valores que recibirá
	 * por parámetro
	 * 
	 * @param x
	 * @param y
	 * @return devuelve un float
	 */
	float resta(float x, float y);

	/**
	 * Este método se encargará de realizar la multiplicación entre dos valores que
	 * recibirá por parámetro
	 * 
	 * @param x
	 * @param y
	 * @return devuelve un float
	 */
	float multiplicacion(float x, float y);

	/**
	 * Este método se encargará de realizar la división entre dos valores que
	 * recibirá por parámetro
	 * @param x
	 * @param y
	 * @return
	 */
	float division(float x, float y);
}

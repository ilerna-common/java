package main.java.tema6.calculadora.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CalculadoraDaoImpl implements CalculadoraDao {

	@Override
	public void guardar(float resultado) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			// Paso 1: Realizamos la conexion
			conn = conector();
			System.out.println("Nos hemos conectado a la BBDD");
			String sql = "INSERT INTO RESULTADO (id, resultado) VALUES (22, " + resultado + ")";
			stmt.executeUpdate(sql);

		} catch (SQLException se) {
			// Gestionamos los posibles errores que puedan surgir durante la ejecucion de la
			// inserci�n
			se.printStackTrace();
		} catch (Exception e) {
			// Gestionamos los posibles errores
			e.printStackTrace();
		} finally {
			// Cerramos la connexion
			try {

				conn.close();
			} catch (SQLException se) {
				System.out.println("No se ha podido cerrar la conexión.");
			}
		}

	}

	private Connection connect = null;
	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/ilerna";
	static final String USUARIO = "alumno";
	static final String CONTA = "password";

	private Connection conector() throws Exception {
		try {
			// Esto se encarga de cargar el driver de MySQL
			Class.forName(JDBC_DRIVER);
			// Establecemos la conexion
			connect = DriverManager.getConnection(DB_URL + "user=" + USUARIO + " &password=" + CONTA);

		} catch (Exception e) {
			throw e;
		} finally {
			connect.close();
		}
		return connect;
	}

}

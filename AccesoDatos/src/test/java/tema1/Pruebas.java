package test.java.tema1;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Rule;
import org.junit.gen5.api.Test;
import org.junit.rules.TemporaryFolder;

public class Pruebas {
	
	//Objeto necesario para crear y gestionar ficheros y carpetas
	 @Rule
	 public TemporaryFolder tempFolder = new TemporaryFolder();
	
	 
	 
	@Test
    public void testCrearDirectorio() throws IOException{
        File archivo = tempFolder.newFolder("carpetaPrueba");
        
        //Comprueba si esa carpeta existe y devuelve un true
        assertTrue(archivo.exists());
    }
     
    @Test
    public void testBorrarDirectorio() throws IOException{
        File archivo = tempFolder.newFile("carpetaPrueba");
        archivo.delete();
        //Comprueba si esa carpeta existe y devuelve un false si todo ha ido bien
        assertFalse(archivo.exists());
    }
	
    //Es la etiqueta que define el metodo como un metodo test
    @Test
    public void testCreacion() throws IOException{
        File archivo = tempFolder.newFile("prueba.txt");
        assertTrue(archivo.exists());
    }
	     
    @Test
    public void testBorrado() throws IOException{
        File archivo = tempFolder.newFile("prueba.txt");
        archivo.delete();
        assertFalse(archivo.exists());
    }
	     
   

}

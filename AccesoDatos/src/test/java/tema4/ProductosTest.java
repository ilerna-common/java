package test.java.tema4;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import main.java.tema4.EjercicicosDaoImpl;

public class ProductosTest {
	
	/**
	 * test que comprueba si el método creacionTablasObjetos() de EjerciciosDaoImpl 
	 * funciona correctamente
	 */
	@Test
	public void testCreacion() {
		boolean b = true;
		EjercicicosDaoImpl dao = new EjercicicosDaoImpl();

		//Si funciona bien, dará true.
		assertTrue(dao.creacionTablasObjetos());
	}
	
	@Test
	public void testSelect() {
		EjercicicosDaoImpl dao = new EjercicicosDaoImpl();
		assertTrue(dao.seleccionObjetos());
	}
}

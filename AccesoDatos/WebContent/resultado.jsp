<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="src.java.calculadora.bean.*, javax.naming.*"%>

<%!
    private CalculadoraBean calculadora = null;
    float result = 0;

    public void jspInit() {
        try {

            InitialContext ic = new InitialContext();
            calculadora = (CalculadoraBean)ic.lookup(CalculadoraBean.class.getName());
            

            System.out.println("Loaded Calculator Bean");


        } catch (Exception ex) {
            System.out.println("Error:"
                    + ex.getMessage());
        }
    }

    public void jspDestroy() {
    	calculadora = null;
    }
%>


<%

    try {
        String s1 = request.getParameter("num1");
        String s2 = request.getParameter("num2");
        String s3 = request.getParameter("operacion");

        System.out.println(s3);

        if (s1 != null && s2 != null) {
            Float num1 = new Float(s1);
            Float num2 = new Float(s2);

            if (s3.equals("suma")) {
                result = calculadora.suma(num1.floatValue(), num2.floatValue());
            } else if (s3.equals("resta")) {
                result = calculadora.resta(num1.floatValue(), num2.floatValue());
            } else if (s3.equals("multi")) {
                result = calculadora.multiplicacion(num1.floatValue(), num2.floatValue());
            } else {
                result = calculadora.division(num1.floatValue(), num2.floatValue());
            }

%>
<p>
    <b>El resultado es:</b> <%= result%>
<p>

    <%
            }
        }// end of try
        catch (Exception e) {
            e.printStackTrace();
            //result = "Not valid";
        }
    %>

